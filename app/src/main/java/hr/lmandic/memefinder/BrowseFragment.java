package hr.lmandic.memefinder;

import android.app.SearchManager;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.ActionBarContainer;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.Toast;


import com.google.firebase.database.Query;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;


public class BrowseFragment extends Fragment implements CustomClickListener{

    private static final String TAG ="BrowseFragment" ;
    private RecyclerView recycler;
    private RecyclerAdapter adapter;
    private DatabaseManager dbManager;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
            View recyclerView=inflater.inflate(R.layout.fragment_browse, container, false);
            setupRecycler(recyclerView);
            setupRecyclerData();
            setHasOptionsMenu(true);
            return recyclerView;

    }
    public void filterItems(String query){
        adapter.getFilter().filter(query);
    }


    private void setupRecyclerData() {

        dbManager=new DatabaseManager("images");

        setUpDocuments(dbManager.getCollectionReference());



    }

    private  void setUpDocuments(CollectionReference reference){
        List<Image> dataList= new ArrayList<Image>();
        dbManager.setRegistration(dbManager.getCollectionReference().addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(QuerySnapshot queryDocumentSnapshots, FirebaseFirestoreException e) {
                if (e != null) {
                    Log.d(TAG,"exception");
                    return;
                }

                for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {

                    Image image= documentSnapshot.toObject(Image.class);
                    image.setDocumentId(documentSnapshot.getId());

                    dataList.add(image);
                }

                adapter.addData(dataList);



            }
        }));

    }



    private void setupRecycler(View recyclerView) {

        recycler=recyclerView.findViewById(R.id.recyclerView);
        LinearLayoutManager mng= new LinearLayoutManager(getActivity());
        recycler.setLayoutManager(mng);
        adapter=new RecyclerAdapter(this);
        recycler.setAdapter(adapter);

    }
    @Override
    public void onStart() {
        super.onStart();


    }

    @Override
    public void onPause() {
        super.onPause();
        dbManager.getRegistration().remove();
    }

    @Override
    public void onClick(int index) {
        Toast.makeText(getContext(),"Meme #"+(index+1),Toast.LENGTH_SHORT).show();
    }


}