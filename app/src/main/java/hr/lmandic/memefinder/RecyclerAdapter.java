package hr.lmandic.memefinder;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<ItemViewHolder> implements Filterable {

    private ArrayList<Image> dataList = new ArrayList<Image>();
    private ArrayList<Image> dataListFull;
    private CustomClickListener clickListener;

    public RecyclerAdapter(CustomClickListener clickListener)
    {
        this.clickListener=clickListener;
    }
    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View listItemView= LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new ItemViewHolder(listItemView, clickListener);

    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        holder.setItem(dataList.get(position));
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
    public void addData(List<Image> array){
        dataList.clear();
        dataList.addAll(array);
        dataListFull = new ArrayList<Image>(dataList);
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        return imageFilter;
    }
    private Filter imageFilter= new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) { //nece zamrznuti apk
            List<Image> filteredList=new ArrayList<>();
            if(constraint==null || constraint.length()==0){
                filteredList.addAll(dataListFull);
            }
            else{
                String filterPattern=constraint.toString().toLowerCase().trim();
                for(Image image: dataListFull){
                    for(String tag : image.getTags()){
                        if(tag.toLowerCase().contains(filterPattern)){
                            filteredList.add(image);
                            break;
                        }
                    }
                }
            }
            FilterResults results= new FilterResults();
            results.values=filteredList;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if(results.values!=null){
                dataList.clear();
                dataList.addAll((List) results.values);
                notifyDataSetChanged();}
        }
    };
}
