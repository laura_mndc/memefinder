package hr.lmandic.memefinder;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {
    private DrawerLayout drawerLayout;
    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    private TextView appNameTV;
    private SearchView searchSV;
    private BrowseFragment browseFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        setUpViewPager();
        appNameTV =findViewById(R.id.appNameTV);
        searchSV = findViewById(R.id.searchSV);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchSV.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchSV.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (view.hasFocus() || searchSV.getQuery().length() > 0) {
                    appNameTV.setVisibility(View.GONE);
                } else {
                    appNameTV.setVisibility(View.VISIBLE);
                }
            }
        });
        searchSV.setOnQueryTextListener(this);
    }

    private void setUpViewPager() {
        List<Fragment> fragmentList=new ArrayList();
        browseFragment=new BrowseFragment();
        fragmentList.add(browseFragment);
        fragmentList.add(new MemeOfTheDayFragment());
        fragmentList.add(new UploadFragment());

        SlidePagerAdapter adapter=new SlidePagerAdapter(getSupportFragmentManager(),fragmentList);
        mViewPager.setAdapter(adapter);
        mTabLayout.setupWithViewPager(mViewPager);
    }

    private void initViews() {
        mViewPager=findViewById(R.id.viewPager);
        mTabLayout=findViewById(R.id.tabLayout);
        drawerLayout=findViewById(R.id.drawer_layout);
    }
    public void clickMenu(View view){
        openDrawer(drawerLayout);
    }

    public static void openDrawer(DrawerLayout drawerLayout) {
        drawerLayout.openDrawer(GravityCompat.START);
    }
    public void clickText(View view){
        closeDrawer(drawerLayout);
    }

    public static void closeDrawer(DrawerLayout drawerLayout) {
        if(drawerLayout.isDrawerOpen(GravityCompat.START)){
            drawerLayout.closeDrawer(GravityCompat.START);
        }
    }
    public void clickAll(View view){
        browseFragment.filterItems("");
        closeDrawer(drawerLayout);
        mViewPager.setCurrentItem(0);
    }
    public void clickArt(View view){
        browseFragment.filterItems("art");
        closeDrawer(drawerLayout);
        mViewPager.setCurrentItem(0);
    }
    public void clickMusic(View view){
        browseFragment.filterItems("music");
        closeDrawer(drawerLayout);
        mViewPager.setCurrentItem(0);
    }
    public void clickFood(View view){
        browseFragment.filterItems("food");
        closeDrawer(drawerLayout);
        mViewPager.setCurrentItem(0);
    }

    public void clickSport(View view){
        browseFragment.filterItems("sport");
        closeDrawer(drawerLayout);
        mViewPager.setCurrentItem(0);
    }
    public void clickTechnology(View view){
        browseFragment.filterItems("technology");
        closeDrawer(drawerLayout);
        mViewPager.setCurrentItem(0);
    }
    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        mViewPager.setCurrentItem(0);
        browseFragment.filterItems(newText);

        return false;

    }


    protected void onPause(){
        super.onPause();
        closeDrawer(drawerLayout);
    }
}