package hr.lmandic.memefinder;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.List;

public class SlidePagerAdapter extends FragmentStatePagerAdapter {
    private List<Fragment> mItems;


    public SlidePagerAdapter(@NonNull FragmentManager fm, List<Fragment> fragmentList) {
        super(fm,BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        mItems=fragmentList;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return mItems.get(position);
    }



    @Override
    public int getCount() {
        return mItems.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        if(position==0){
            return "Browse";
        }
        else if(position==1){
            return "Meme Of The Day";
        }
       return "Upload";
    }
}
