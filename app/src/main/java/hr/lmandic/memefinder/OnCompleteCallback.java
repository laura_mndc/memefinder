package hr.lmandic.memefinder;

import java.util.List;

public interface OnCompleteCallback {
    void onComplete(List<Image> images);
}
