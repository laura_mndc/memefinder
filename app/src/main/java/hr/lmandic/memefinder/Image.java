package hr.lmandic.memefinder;

import com.google.firebase.firestore.Exclude;

import java.util.ArrayList;

public class Image {
    private String documentId;



    private int numberId;
    private String imageURL;
    private ArrayList<String> tags;
    public Image(){

    }
    public Image(String image,int numberId,ArrayList<String> tags){
        this.imageURL=image;
        this.tags=tags;
        this.numberId=numberId;
    }
    @Exclude
    public String getDocumentId() {
        return documentId;
    }
    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public ArrayList<String> getTags() {
        return tags;
    }

    public void setTags(ArrayList<String> tags) {
        this.tags = tags;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String image) {
        this.imageURL = image;
    }

    public int getNumberId() {
        return numberId;
    }

    public void setNumberId(int numberId) {
        this.numberId = numberId;
    }
}
