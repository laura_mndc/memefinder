package hr.lmandic.memefinder;

public interface CustomClickListener {
    void onClick(int position);
}
