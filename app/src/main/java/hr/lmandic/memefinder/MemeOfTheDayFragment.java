package hr.lmandic.memefinder;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Query;
import com.squareup.picasso.Picasso;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MemeOfTheDayFragment extends Fragment {

    private ImageView imageView;
    private DatabaseManager dbManagerDaily;
    private DatabaseManager dbManagerImages;
    private DocumentReference docRef;
    int lastId;
    private static final String TAG = "MemeOfTheDayFragment";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_meme_of_the_day, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        dbManagerImages=new DatabaseManager("images");
        dbManagerDaily=new DatabaseManager("daily");
        imageView=view.findViewById(R.id.ivDailyMeme);
        docRef= dbManagerDaily.getCollectionReference().document("lastRefresh");
        final Timestamp[] lastTime = new Timestamp[1];
        //final int[] lastId = new int[1];
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                        lastTime[0] = document.getTimestamp("time");
                        lastId=document.getLong("imageId").intValue();
                        setUpDailyPicture(lastTime[0]);
                    } else {
                        Log.d(TAG, "No such document");
                    }
                } else {
                    Log.d(TAG, "get failed with ", task.getException());
                }
            }
        });






    }

    private void setUpDailyPicture(Timestamp lastTime) {
        Date lastDate=lastTime.toDate();

        int lastday = ZonedDateTime.ofInstant(lastDate.toInstant(),
                ZoneId.of("Europe/Zagreb")).getDayOfMonth();
        ZonedDateTime now=ZonedDateTime.now().withZoneSameInstant(ZoneId.of("Europe/Zagreb"));
        int daytoday=now.getDayOfMonth();
        if(lastday!=daytoday){

            Query query=dbManagerImages.getCollectionReference().orderBy("numberId", Query.Direction.DESCENDING).limit(1);

            dbManagerImages.getImagesFromDatabase(query, new OnCompleteCallback() {
                @Override
                public void onComplete(List<Image> images) {
                    Image maxImage=images.get(0);
                    int maxNumberId=maxImage.getNumberId();
                    lastId= (int) (Math.random()*maxNumberId); //lastidrefresh
                    setNewDaily();
                    getDailyPicture();
                }
            });



        }
        else{
            getDailyPicture();
        }
    }

    private void getDailyPicture() {
        Query query=dbManagerImages.getCollectionReference().whereGreaterThanOrEqualTo("numberId",lastId);
        dbManagerImages.getImagesFromDatabase(query, new OnCompleteCallback() {
            @Override
            public void onComplete(List<Image> images) {
                Image imageofday=images.get(0);
                Picasso.get().load(imageofday.getImageURL()).into(imageView);
            }
        });

    }

    public void setNewDaily(){
        Map<String, Object> refresh = new HashMap<>();
        refresh.put("imageId", lastId);
        refresh.put("time", Timestamp.now());
        docRef.set(refresh).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.d(TAG, "DocumentSnapshot successfully written!");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.w(TAG, "Error writing document", e);
            }
        });
    }

}