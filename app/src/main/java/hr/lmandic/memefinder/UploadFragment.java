package hr.lmandic.memefinder;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipDrawable;
import com.google.android.material.chip.ChipGroup;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;


public class UploadFragment extends Fragment {
    private static final String TAG = "UploadFragment";
    private ImageView ivPreview;
    private Button btnSelect;
    private Button btnUpload;
    private EditText etTags;
    private ChipGroup mChipGroup;


    private StorageReference mStorageRef;
    private ProgressDialog mProgressDialog;
    private String thumb_download_url;
    private Uri selectedImageURI;
    private String fileName;
    private DatabaseManager dbManager=new DatabaseManager("images");




    private static int RESULT_LOAD_IMAGE = 1;//u slucaju da je triggerano vise Intentova iz istog activityja i od svakog ocekujemo rez

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_upload, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ivPreview=(ImageView)view.findViewById(R.id.ivPreview);
        btnUpload= view.findViewById(R.id.btnUpload);
        btnSelect= view.findViewById(R.id.btnSelect);
        etTags=view.findViewById(R.id.etTags);
        mChipGroup=view.findViewById(R.id.chipGroup);

        mProgressDialog = new ProgressDialog(getContext());
        mStorageRef = FirebaseStorage.getInstance().getReference();
        checkFilePermissions();

        etTags.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN)
                {
                    switch (keyCode)
                    {

                        case KeyEvent.KEYCODE_ENTER:
                            addTag(etTags.getText().toString());
                            etTags.setText("");
                            return true;
                        default:
                            break;
                    }
                }
                return false;
            }

            private void addTag(String text) {
                Chip chip=new Chip(getContext());
                ChipDrawable drawable=ChipDrawable.createFromAttributes(getContext(),null,0,R.style.Widget_MaterialComponents_Chip_Entry);
                chip.setChipDrawable(drawable);
                chip.setCheckable(false);
                chip.setClickable(false);
                chip.setText(text);
                chip.setOnCloseIconClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mChipGroup.removeView(chip);
                    }
                });
                mChipGroup.addView(chip);

            }
        });
        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: Uploading Image.");
                mProgressDialog.setMessage("Uploading Image...");
                mProgressDialog.show();

                uploadImageToStorage();


            }
        });
        btnSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { //otvara apk chooser
                Intent i = new Intent(
                        Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_IMAGE);
            }
        });

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) { //dobija uri slike i postavlja preview
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
           selectedImageURI = data.getData();
            ivPreview.setImageURI(selectedImageURI);

            Cursor returnCursor =
                    getContext().getContentResolver().query(selectedImageURI, null, null, null, null);
            /*
             * Get the column indexes of the data in the Cursor,
             * move to the first row in the Cursor, get the data,
             * and display it.
             */
            int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);

            returnCursor.moveToFirst();
            fileName=returnCursor.getString(nameIndex);
            returnCursor.close();
        }
    }
    private void uploadImageToStorage() { //upload slike u Firebase Storage, postavlja thumb_download_url


        if(!fileName.equals("")){

            StorageReference storageReference = mStorageRef.child("Memes/" +fileName); //Uzeti nov naziv za storage
            final UploadTask uploadTask = storageReference.putFile(selectedImageURI); //dobar novi uri
            uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                    uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                        @Override
                        public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                            if (!task.isSuccessful()) {
                                throw task.getException();

                            }
                            // Continue with the task to get the download URL
                            Task<Uri> downloadurl= storageReference.getDownloadUrl(); //Task<Uri>
                            return downloadurl;

                        }
                    }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                        @Override
                        public void onComplete(@NonNull Task<Uri> task) {
                            if (task.isSuccessful()) {
                                thumb_download_url = task.getResult().toString();
                                saveImage();

                            }
                        }
                    });
                    mProgressDialog.dismiss();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    toastMessage("Upload failed");
                    mProgressDialog.dismiss();
                }
            });
        }

    }
    public void saveImage() {

        ArrayList<String> tags=new ArrayList<>();
        for (int i = 0; i < mChipGroup.getChildCount(); i++) {
            String tag = ((Chip) mChipGroup.getChildAt(i)).getText().toString();
            tags.add(tag);
        }
        Query query=dbManager.getCollectionReference().orderBy("numberId", Query.Direction.DESCENDING).limit(1);

        dbManager.getImagesFromDatabase(query, new OnCompleteCallback() {
            @Override
            public void onComplete(List<Image> images) {
                Image maxImage=images.get(0);
                int numberId=maxImage.getNumberId()+1;
                Image image = new Image(thumb_download_url,numberId,tags);
                dbManager.saveImage(fileName,image);
            }
        });

    }



    private void checkFilePermissions() {

        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.M){
            int permissionCheck = getActivity().checkSelfPermission("Manifest.permission.READ_EXTERNAL_STORAGE");
            permissionCheck +=  getActivity().checkSelfPermission("Manifest.permission.WRITE_EXTERNAL_STORAGE");
            if (permissionCheck != 0) {
                this.requestPermissions(new String[]{android.Manifest.permission.MANAGE_EXTERNAL_STORAGE,android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE}, 1001); //Any number
            }
        }else{
            Log.d(TAG, "checkBTPermissions: No need to check permissions. SDK version < MARSHMALLOW.");
        }
    }
    private void toastMessage(String message){
        Toast.makeText(getContext(),message, Toast.LENGTH_SHORT).show();
    }


}