package hr.lmandic.memefinder;

import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.chip.Chip;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class DatabaseManager {
    private static final String TAG ="DatabaseManager";
    private FirebaseFirestore db;
    private CollectionReference collectionReference;
    private ListenerRegistration registration;

    public DatabaseManager(String collectionPath) {
        db= FirebaseFirestore.getInstance();
        collectionReference=db.collection(collectionPath);

    }
    public FirebaseFirestore getDb() {
        return db;
    }

    public void setDb(FirebaseFirestore db) {
        this.db = db;
    }

    public CollectionReference getCollectionReference() {
        return collectionReference;
    }

    public void setCollectionReference(CollectionReference collectionReference) {
        this.collectionReference = collectionReference;
    }

    public ListenerRegistration getRegistration() {
        return registration;
    }

    public void setRegistration(ListenerRegistration registration) {
        this.registration = registration;
    }

    public List<Image> getAllImagesFromDatabase(){

        List<Image> dataList= new ArrayList<Image>();
        collectionReference.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        Image image=document.toObject(Image.class);
                        image.setDocumentId(document.getId());
                        dataList.add(image);
                    }
                } else {
                    Log.d("DatabaseManager", "Error getting documents: ", task.getException());
                }
            }
        });


        return dataList;
    }
    public void getImagesFromDatabase(Query query,OnCompleteCallback callback){

        List<Image> dataList= new ArrayList<Image>();
        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        Image image=document.toObject(Image.class);
                        image.setDocumentId(document.getId());
                        dataList.add(image);
                    }
                } else {
                    Log.d("DatabaseManager", "Error getting documents: ", task.getException());
                }
                callback.onComplete(dataList);

            }
        });
       // return dataList;
    }




    public void saveImage(String fileName,Image image) {
        //upload u Firestore
        DocumentReference docRef=collectionReference.document(fileName);

        docRef.set(image)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "Document saved to Firestore.");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                        Log.d(TAG, e.toString());
                    }
                });
    }

}

