package hr.lmandic.memefinder;

import android.util.Log;
import android.view.View;

import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import com.squareup.picasso.Picasso;
public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private TextView liTextView;
    private ImageView liImageView;
    private CustomClickListener clickListener;


    public ItemViewHolder(@NonNull View itemView, CustomClickListener listener) {
        super(itemView);
        this.clickListener = listener;
        liTextView = itemView.findViewById(R.id.liTextView);
        liImageView=itemView.findViewById(R.id.liImageView);
        itemView.setOnClickListener(this);

    }

    public void setItem(Image image) {

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < image.getTags().size(); i++) {
            builder.append(image.getTags().get(i));

            if(i<image.getTags().size()-1)   {
                builder.append(", ");
            }
            
        }
        liTextView.setText(builder.toString());

        Picasso.get().load(image.getImageURL()).into(liImageView);

    }

    @Override
    public void onClick(View v) {
        int position = getAdapterPosition();
        clickListener.onClick(position);
    }

}